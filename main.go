package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", getSum)

	err := http.ListenAndServe(":3333", nil)
	if err != nil {
		fmt.Printf("Server Err : %v", err)
	}
}

func getSum(rw http.ResponseWriter, r *http.Request) {
	fmt.Println(Sum(1, 2))
}

func Sum(a, b int) int {
	return a + b
}
