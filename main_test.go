package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSum(t *testing.T) {
	var v int
	v = Sum(1, 2)
	assert.Equal(t, 3, v)
}
